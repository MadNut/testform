$(document).ready(function(){
	var firstSumOrder = 0; var secondSumOrder = 0;
	var saladSumOrder = 0; var drinkSumOrder = 0; 
	var breadOrder = 0;    var summor = 0;
	var donutSumOrder = 0; var garnishSumOrder = 0;

	var textFirst = "";    var textSecond = "";
	var textSalad = "";    var textDrink = "";
	var textBread = "";    var textSugar = "";
	var textOr = "";       var textDonut = "";
	var textDoneness = ""; var textGarnish = "";

//функции подсчета стоимости и вывода заказанного

function summ(){
	summor = firstSumOrder+secondSumOrder+saladSumOrder+drinkSumOrder+breadOrder+donutSumOrder+garnishSumOrder;
	$("#sum").text(summor);
}
function textOrder(){
	textOr = textFirst+" "+textDonut+" "+textSecond+" "+textDoneness+" "+textGarnish+" "+textSalad+" "+textDrink+" "+textSugar+" "+textBread;
	$("#ord").text(textOr);
}

//Выбор первого блюда
	$(function(){
		$("#f1").change(function(){
			$("#f1").slideUp(250);
			$("#f1").slideDown(250);
			firstSumOrder = 0;
			firstSumOrder = 100;
			summ();
			textFirst = "";
			textFirst = "Борщ";
			textOrder();
		});
		$("#f2").change(function(){
			if($("#f2").is(":checked")) {
				donutSumOrder = 0;
				donutSumOrder = 5;
				summ();
				textDonut = "";
				textDonut = "Помпушка";
				textOrder();
			} else {
				donutSumOrder = 0;
				summ();
				textDonut = "";
				textOrder();
			}
				
		});
		$("#f3").change(function(){
			$("#f3").slideUp(250);
			$("#f3").slideDown(250);
			firstSumOrder = 0;
			firstSumOrder = 70;
			summ();
			textFirst = "";
			textFirst = "Сырный суп";
			textOrder();
		});
		$("#f4").change(function(){
			$("#f4").slideUp(250);
			$("#f4").slideDown(250);
			firstSumOrder = 0;
			firstSumOrder = 60;
			summ();
			textFirst = "";
			textFirst = "Грибной суп";
			textOrder();
		});
		$("#f5").change(function(){
			$("#f5").slideUp(250);
			$("#f5").slideDown(250);
			firstSumOrder = 0;
			summ();
			textFirst = "";
			textOrder();
		});
	});

//выбор второго блюда
	$(function(){
		$("#sec1").change(function(){
			$("#sec1").slideUp(250);
			$("#sec1").slideDown(250);
			$("#DN").css("visibility","hidden");
			secondSumOrder = 0;
			secondSumOrder = 100;
			summ();
			textSecond = "";
			textSecond = "Курица";
			textDoneness = "";
			textOrder();
		});
		$("#sec2").change(function(){
			$("#sec2").slideUp(250);
			$("#sec2").slideDown(250);
			$("#DN").css("visibility","hidden");
			secondSumOrder = 0;
			secondSumOrder = 120;
			summ();
			textSecond = "";
			textSecond = "Рыба";
			textDoneness = "";
			textOrder();
		});
		$("#sec3").change(function(){
			$("#sec3").slideUp(250);
			$("#sec3").slideDown(250);
			$("#DN").css("visibility","visible");
			$("#doneness").change(function(){
			var done = $("#doneness").val();
			if(done == "rare"){
				textDoneness = "";
				textDoneness = "(с кровью)";
				textOrder();
			};
			if(done == "medium"){
				textDoneness = "";
				textDoneness = "(полупрожаренная)";
				textOrder();
			};
			if(done == "wellDone"){
				textDoneness = "";
				textDoneness = "(прожаренная)";
				textOrder();
			};
		    });
			secondSumOrder = 0;
			secondSumOrder = 150;
			summ();
			textSecond = "";
			textSecond = "Свинина";
			textOrder();
		});
		$("#sec4").change(function(){
			$("#sec4").slideUp(250);
			$("#sec4").slideDown(250);
			$("#DN").css("visibility","visible");
			$("#doneness").change(function(){
			var done = $("#doneness").val();
			if(done == "rare"){
				textDoneness = "";
				textDoneness = "(с кровью)";
				textOrder();
			};
			if(done == "medium"){
				textDoneness = "";
				textDoneness = "(полупрожаренная)";
				textOrder();
			};
			if(done == "wellDone"){
				textDoneness = "";
				textDoneness = "(прожаренная)";
				textOrder();
			};
		    });
			secondSumOrder = 0;
			secondSumOrder = 150;
			summ();
			textSecond = "";
			textSecond = "Говядина";
			textOrder();
			
		});
		$("#sec5").change(function(){
			$("#sec5").slideUp(250);
			$("#sec5").slideDown(250);
			$("#DN").css("visibility","hidden");
			secondSumOrder = 0;
			summ();
			textSecond = "";
			textDoneness = "";
			textOrder();
		});
		
		
		$("#garnish").change(function(){
			var garn = $("#garnish").val();
			if(garn == "missGarnish"){
				garnishSumOrder = 0;
				summ();
				textGarnish = "";
				textOrder();
			};
			if(garn == "rice"){
				garnishSumOrder = 0;
				garnishSumOrder = 50;
				summ();
				textGarnish = "";
				textGarnish = "Рис";
				textOrder();
			};
			if(garn == "fries"){
				garnishSumOrder = 0;
				garnishSumOrder = 30;
				summ();
				textGarnish = "";
				textGarnish = "Картофель фри";
				textOrder();
			};
			if(garn == "grilledVeg"){
				garnishSumOrder = 0;
				garnishSumOrder = 30;
				summ()
				textGarnish = "";
				textGarnish = "Овощи на гриле";
				textOrder();
			};
		});
	});
	
//Выбор салата
	$(function(){
		$("#s1").change(function(){
			$("#s1").slideUp(250);    //эффект кнопки
			$("#s1").slideDown(250);
			saladSumOrder = 0;
			$("#sch1").css("visibility","hidden");  
			saladSumOrder = 100;
			summ();
			textSalad = " ";
			textSalad = "Греческий салат";
			textOrder();
		});
		$("#s2").change(function(){
			$("#s2").slideUp(250);
			$("#s2").slideDown(250);
			saladSumOrder=0;
			$("#sch1").css("visibility","visible");
			$("#sch1").change(function(){
				var op = $("#sch1").val();
				if(op == "chick") {
					saladSumOrder = 0;
					saladSumOrder = 100;
					summ();
					textSalad = " ";
					textSalad = "Цезарь с курицей";
					textOrder();
				};
				if(op == "shrimp") {
					saladSumOrder = 0;
					saladSumOrder = 150;
					summ();
					textSalad = " ";
					textSalad = "Цезарь с креветками";
					textOrder();
				};
			});
		});
		$("#s3").change(function(){
			$("#s3").slideUp(250);
			$("#s3").slideDown(250);
			saladSumOrder = 0;
			$("#sch1").css("visibility","hidden");
			summ();
			textSalad = " ";
			textOrder();
		});
	});
	
//Выбор напитка
	$(function(){
//сок		
		$("#d1").change(function(){
			$("#d1").slideUp(250);
			$("#d1").slideDown(250);
			drinkSumOrder = 0;
			$("#sch4").css("visibility","visible");
			$("#sch5").css("visibility","hidden");
			$("#sch6").css("visibility","hidden");
			$("#sch4").change(function(){
				var op1 = $("#sch4").val();
				if(op1 == "orange") {
					drinkSumOrder = 0;
					drinkSumOrder = 50;
					summ();
					textDrink = " ";
					textDrink = "Апельсиновый сок";
					textOrder();
				};
				if(op1 == "apple") {
					drinkSumOrder = 0;
					drinkSumOrder = 50;
					summ();			
					textDrink = " ";
					textDrink = "Яблочный сок";
					textOrder();
				};
			});
		});
//кофе
		$("#d2").change(function(){
			$("#d2").slideUp(250);
			$("#d2").slideDown(250);
			drinkSumOrder = 0;
			$("#sch5").css("visibility","visible");
			$("#sugarC").css("visibility","visible");
			$("#sugarT").css("visibility","hidden");
			$("#sch4").css("visibility","hidden");
			$("#sch6").css("visibility","hidden");
			$("#sch5").change(function(){
				var op2 = $("#sch5").val();
				if(op2 == "espresso") {
					drinkSumOrder = 0;
					drinkSumOrder = 80;
					summ();
					textDrink = " ";
					textDrink = "Эспрессо";
					textOrder();
				};
				if(op2 == "americano") {
					drinkSumOrder = 0;
					drinkSumOrder = 80;
					summ();		
					textDrink = " ";
					textDrink = "Американо";
					textOrder();			
				};
			});		
		});
		$("#sugarCoffee").change(function(){
			if ($('#sugarCoffee').is(':checked')) {
				textSugar = " ";
				textSugar = "Сахар";
				textOrder();
			} else{
				textSugar = " ";
				textOrder();
			}
		});		
//чай
		$("#d3").change(function(){
			$("#d3").slideUp(250);
			$("#d3").slideDown(250);
			drinkSumOrder = 0;
			$("#sch6").css("visibility","visible");
			$("#sugarT").css("visibility","visible");
			$("#sugarC").css("visibility","hidden");
			$("#sch4").css("visibility","hidden");
			$("#sch5").css("visibility","hidden");
			$("#sugarTea").change(function(){
				var sugarT = $("#sugarTea").val();
				if(sugarT == "sugarFree"){
					textSugar = " ";
					textSugar = "Сахар";
					textOrder();
				};			
			});
			$("#sch6").change(function(){
				var op3 = $("#sch6").val();
				if(op3 == "blackTea") {
					drinkSumOrder = 0;
					drinkSumOrder = 30;
					summ();
					textDrink = " ";
					textDrink = "Чёрный чай";
					textOrder();
				};
				if(op3 == "greenTea") {
					drinkSumOrder = 0;
					drinkSumOrder = 30;
					summ();					
					textDrink = " ";
					textDrink = "Зелёный чай";
					textOrder();
				};
			});
			$("#sugarTea").change(function(){
			if ($('#sugarTea').is(':checked')) {
				textSugar = " ";
				textSugar = "Сахар";
				textOrder();
			} else{
				textSugar = " ";
				textOrder();
			}
		});
		});
//Напиток не выбран
		$("#d4").change(function(){
			$("#d4").slideUp(250);
			$("#d4").slideDown(250);
			drinkSumOrder = 0;
			$("#sugarC").css("visibility","hidden");
			$("#sugarT").css("visibility","hidden");
			$("#sch4").css("visibility","hidden");
			$("#sch5").css("visibility","hidden");
			$("#sch6").css("visibility","hidden");
			summ();
			textDrink = " ";
			textSugar = " ";
			textOrder();
		});
//хлеб
		$("#bread").change(function(){
			var b = $("#bread").val()
			if(b == "missBread"){
				breadOrder = 0;
				summ();
				textBread = " ";
				textOrder();
			};
			if(b == "whiteBread"){
				breadOrder = 0;
				breadOrder = 10; 
				summ();
				textBread = " ";
				textBread = "Белый хлеб";
				textOrder();
			};
			if(b == "blackBread"){
				breadOrder = 0;
				breadOrder = 10;
				summ();
				textBread = " ";
				textBread = "Чёрный хлеб";
				textOrder();
			};
		});		
	});

//данные с формы
	$("#but").click(function(){
		$("#formOrder").slideDown(1000);
    	var msg = ($("#customerName, #customerFam, #customerPhone").serialize());
		alert(msg);
	});	



});